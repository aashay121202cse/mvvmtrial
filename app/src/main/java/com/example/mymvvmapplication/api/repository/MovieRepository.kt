package com.example.mymvvmapplication.api.repository

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.mymvvmapplication.PopularMovies
import com.example.mymvvmapplication.api.network.MyRetrofitInterface
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Callback

class MovieRepository {

    fun getListOfMovies() : LiveData<String> {
        Log.i("MianActivity", "inside movie repository")

        val apiResponse = MutableLiveData<String>()

        MyRetrofitInterface().getMovies(email = "probelalkhan@gmail.com", password = "123456")
            .enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>, t: Throwable) {
                    print(t.message)
                }

                override fun onResponse(
                    call: Call<String>,
                    response: Response<String>
                ) {
                    print(response.toString())
apiResponse.value = response.body()
                }

            })

        return apiResponse
    }
}