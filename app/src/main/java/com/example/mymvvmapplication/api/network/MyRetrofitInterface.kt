package com.example.mymvvmapplication.api.network

import com.example.mymvvmapplication.PopularMovies
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface MyRetrofitInterface {
    @POST("login")
    fun getMovies(@Field("email") email: String, @Field("password") password: String): Call<String>

    companion object {
        operator fun invoke() : MyRetrofitInterface {
            val okHttpClient = OkHttpClient.Builder()
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://api.simplifiedcoding.in/course-apis/mvvm/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MyRetrofitInterface::class.java)
        }
    }
}