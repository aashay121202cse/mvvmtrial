package com.example.mymvvmapplication

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mymvvmapplication.api.repository.MovieRepository

class MyViewModel() : ViewModel() {

    val repository: MovieRepository? = null

    fun getListOfMovies() : LiveData<String>? {
        Log.i("MianActivity", "inside viewmodel")
        return repository?.getListOfMovies()
    }

}