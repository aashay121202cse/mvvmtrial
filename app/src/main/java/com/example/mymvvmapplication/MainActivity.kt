package com.example.mymvvmapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var viewModel = ViewModelProviders.of(this).get(MyViewModel::class.java)

viewModel.getListOfMovies()?.observe(this, Observer {
    Log.i("MianActivity", "inside observer " + it)
//    recyclerView.apply {
//        setHasFixedSize(true)
//        layoutManager = LinearLayoutManager(this@MainActivity)
//        adapter = MoviesAdapter(it.results)
//    }

})
    }


}